#ifndef TEMPORALFASTDOWNWARD_DOMAIN_TRANSITION_GRAPH_HELPER_FUNCTIONS_H
#define TEMPORALFASTDOWNWARD_DOMAIN_TRANSITION_GRAPH_HELPER_FUNCTIONS_H

#include "state.h"
#include "operator.h"
#include "axioms.h"
#include "successor_generator.h"
#include "domain_transition_graph.h"
#include "map"
#include "stdlib.h"

int temporal_fd_first(int op_index, int needed_var);

int temporal_fd_last(int op_index, int needed_var);

int temporal_fd_last_grounded(int needed_var);

double temporal_fd_f_at(int op_index, int needed_var);

double temporal_fd_l_at(int op_index, int needed_var);


#endif //TEMPORALFASTDOWNWARD_DOMAIN_TRANSITION_GRAPH_HELPER_FUNCTIONS_H
