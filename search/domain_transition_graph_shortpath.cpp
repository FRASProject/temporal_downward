//conditionalion includes
#include "domain_transition_graph_shortpath.h"
#include "domain_transition_graph.h"
#include "map"



double find_short_path(DomainTransitionGraph* &transition_graphs,
                       map<int, vector<int*>> &shortest_costs_tables, int state1, int state2, const vector<double> &init_states, int var_num) {
    if (transition_graphs->type_graph() == "DomainTransitionGraphSymb") {
        DomainTransitionGraphSymb* t_g = dynamic_cast<DomainTransitionGraphSymb*>(transition_graphs); //only Symb is needed
        int numberOfStates = (int) t_g->nodes.size();
        assert(state1 < numberOfStates);
        assert(state2 < numberOfStates);
        if (shortest_costs_tables.find(var_num) != shortest_costs_tables.end()) {
            if (shortest_costs_tables.find(var_num)->second[state1] == nullptr) {
                int* pathes_from_state1 = new int[numberOfStates];
                t_g->find_shortest_path(state1, pathes_from_state1, init_states);
                shortest_costs_tables.find(var_num)->second[state1] = pathes_from_state1;
            }
        }
        else {
            int* pathes_from_state1 = new int[numberOfStates];
            t_g->find_shortest_path(state1, pathes_from_state1, init_states);
            vector<int*> big_table(numberOfStates, nullptr);
            big_table[state1] = pathes_from_state1;
            shortest_costs_tables.insert(pair<int, vector<int*>>(var_num, big_table));
        }
        int path = shortest_costs_tables.find(var_num)->second[state1][state2];
        return double(path);
    }
    exit(-1); // Exiting to prevent undefined behaviour
}
