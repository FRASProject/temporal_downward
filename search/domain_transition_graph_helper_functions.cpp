//
// Created by xankr on 09.11.2021.
//
#include "domain_transition_graph_helper_functions.h"
#include "temporal_downward_c_api.h"
#include "map"


//whether

int temporal_fd_first(int op_index, int needed_var) { //functions of deciding, whether variables should influence
    // the finding of shortest path

    for (const auto & item: g_operators[op_index].get_prevail_start()) {
        if (item.var == needed_var)
            return item.prev;
    }
    for (const auto & item: g_operators[op_index].get_prevail_overall()) {
        if (item.var == needed_var)
            return item.prev;
    }
    for (const auto & item: g_operators[op_index].get_prevail_end()) {
        if (item.var == needed_var)
            return item.prev;
    }
    for (const auto & item: g_operators[op_index].get_pre_post_start()) {
        if (item.var == needed_var)
            return item.pre;
    }
    exit(-1);
}

int temporal_fd_last_grounded(int needed_var) { //just init state
    return g_initial_state->state[needed_var];
}

int temporal_fd_last(int op_index, int needed_var) {
    for (const auto & item: g_operators[op_index].get_pre_post_end()) {
        if (item.var == needed_var)
            return item.post;
    }
    for (const auto & item: g_operators[op_index].get_pre_post_start()) {
        if (item.var == needed_var)
            return item.post;
    }
    for (const auto & item: g_operators[op_index].get_prevail_end()) {
        if (item.var == needed_var)
            return item.prev;
    }
    for (const auto & item: g_operators[op_index].get_prevail_overall()) {
        if (item.var == needed_var)
            return item.prev;
    }
    for (const auto & item: g_operators[op_index].get_prevail_start()) {
        if (item.var == needed_var)
            return item.prev;
    }
    exit(-1);
}

double temporal_fd_f_at(int op_index, int needed_var) { //variable firstly required by action
    if (g_operators[op_index].get_prevail_start().size() != 0
        or g_operators[op_index].get_prevail_overall().size() != 0
        or g_operators[op_index].get_pre_post_start().size() != 0) {
        for (const auto & item: g_operators[op_index].get_prevail_start()) {
            if (item.var == needed_var)
                return 0;
        }
        for (const auto & item: g_operators[op_index].get_prevail_overall()) {
            if (item.var == needed_var)
                return 0;
        }
        for (const auto & item: g_operators[op_index].get_pre_post_start()) {
            if (item.var == needed_var)
                return 0;
        }
    }
    return temporal_fd_dur(op_index);
}

double temporal_fd_l_at(int op_index, int needed_var) { //variable lastly accessed by action
    if (g_operators[op_index].get_prevail_overall().size() != 0
        or g_operators[op_index].get_prevail_end().size() != 0
        or g_operators[op_index].get_pre_post_end().size() != 0) {
        for (const auto & item: g_operators[op_index].get_prevail_overall()) {
            if (item.var == needed_var)
                return 0;
        }
        for (const auto & item: g_operators[op_index].get_prevail_end()) {
            if (item.var == needed_var)
                return 0;
        }
        for (const auto & item: g_operators[op_index].get_pre_post_end()) {
            if (item.var == needed_var)
                return 0;
        }

    }
    return -temporal_fd_dur(op_index);
}

