#ifndef SUCCESSOR_GENERATOR_H
#define SUCCESSOR_GENERATOR_H

#include <iostream>
#include <vector>

class Operator;
class TimeStampedState;

class SuccessorGenerator
{
    public:
        virtual ~SuccessorGenerator() = default;
        virtual void generate_applicable_ops(const TimeStampedState &curr,
                std::vector<const Operator *> &ops) = 0;
        void dump()
        {
            _dump("  ");
        }
        virtual void _dump(string indent) = 0;
        virtual string type_generator() const = 0;
};

class SuccessorGeneratorSwitch : public SuccessorGenerator
{
    int switch_var;
    SuccessorGenerator *immediate_ops;
    vector<SuccessorGenerator *> generator_for_value;
    SuccessorGenerator *default_generator;
public:
    string type_generator() const override;
    explicit SuccessorGeneratorSwitch(istream &in);
    void generate_applicable_ops(const TimeStampedState &curr,
                                         vector<const Operator *> &ops) override;
    void _dump(string indent) override;
    ~SuccessorGeneratorSwitch() override;
};

class SuccessorGeneratorGenerate : public SuccessorGenerator
{
public:
    vector<const Operator *> op;
    string type_generator() const override;
    explicit SuccessorGeneratorGenerate(istream &in);
    void generate_applicable_ops(const TimeStampedState &curr,
                                         vector<const Operator *> &ops) override;
    void _dump(string indent) override;
    ~SuccessorGeneratorGenerate() override;
};

SuccessorGenerator *read_successor_generator(std::istream &in);

string return_type_generator(SuccessorGenerator* sg);

void delete_inner_gen(SuccessorGenerator* sg);


#endif
