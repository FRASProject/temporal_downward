cmake_minimum_required(VERSION 3.20)
project(search CXX)

set(CMAKE_CXX_STANDARD 11)

file(GLOB search_src CONFIGURE_DEPENDS 
     "*.h"
     "*.cc"
     "*.cpp"
)
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
file(GLOB_RECURSE headers RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/*.h)
foreach(HEADER ${headers})
    string(REGEX MATCH "(.*)[/\\]" DIR ${HEADER})
    file(COPY ${HEADER} DESTINATION ${CMAKE_BINARY_DIR}/include/${DIR})
endforeach(HEADER ${headers})
add_executable(search ${search_src})
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libstdc++")
add_library(temporalsearch SHARED ${search_src})
add_custom_command(TARGET temporalsearch POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
        $<TARGET_FILE:temporalsearch>
        ${CMAKE_CURRENT_SOURCE_DIR}/lib
        )
#target_link_libraries(search "~/TemporalFastDownward/temporal_downward/preprocess/liblpreprocess.so")
#set_target_properties(search PROPERTIES LINKER_LANGUAGE CXX)

