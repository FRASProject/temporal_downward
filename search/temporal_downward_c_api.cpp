#include "temporal_downward_c_api.h"
#include "domain_transition_graph_helper_functions.h"
#include "domain_transition_graph_shortpath.h"
#include <numeric>
#include <sstream>
//global in local threads

void temporal_fd_init_all(const char* input) {
    char* input_moved;
    if (input[0] == '0')
        input_moved = (char*)input + 2; //including special symbol of endl
    else
        input_moved = (char*)input;
    std::string s = input_moved;
    std::stringstream sas_stream;
    sas_stream.str(s);
    cin.rdbuf(sas_stream.rdbuf());
    read_everything(cin);
}

void temporal_fd_deinit() {
    for (auto const& distanceTable: g_shortest_costs_tables)
    {
        for (auto const distancesVector: distanceTable.second) {
            delete(distancesVector);
        }
    }
    g_shortest_costs_tables.clear();
    g_last_arithmetic_axiom_layer = 0;
    g_comparison_axiom_layer = 0;
    g_first_logic_axiom_layer = 0;
    g_last_logic_axiom_layer = 0;

    g_variable_name.clear();
    g_variable_domain.clear();
    g_axiom_layers.clear();
    g_default_axiom_values.clear();
    g_variable_types.clear();
    delete(g_initial_state);
    g_goal.clear();
    g_operators.clear();
    for (Axiom* a: g_axioms) {
        delete(a);
    }
    g_axioms.clear();
    delete(g_axiom_evaluator);
    delete(g_successor_generator);
    for (DomainTransitionGraph* dtg: g_transition_graphs) {
        if (dtg->type_graph() == "DomainTransitionGraphSymb") {
            for (ValueNode node: dynamic_cast<DomainTransitionGraphSymb *>(dtg)->nodes) {
                for (ValueTransition trans: node.transitions) {
                    for (ValueTransitionLabel* lab: trans.ccg_labels) {
                        delete(lab);
                    }
                }
                for (ValueTransition trans: node.additional_transitions) {
                    for (ValueTransitionLabel* lab: trans.ccg_labels) {
                        delete(lab);
                    }
                }
            }
        }
        delete(dtg);
    }
    g_transition_graphs.clear();
    delete(g_causal_graph);
}

template <typename T>
void checker(const T& all_vec, int* out) { //change all our flags in tables, to know which variables should be checked
    //while making tentative distance
    for (const auto* vec: all_vec) {
        for (const auto& item: *vec)
            out[item.var] = 1;
    }
}

vector<double> temporal_fd_grounded_action_dist_interm(int op_index) {
    vector<double> all_dist;
    int precond_vars[g_variable_domain.size()];
    fill_n(precond_vars, g_variable_domain.size(), -1);
    vector<const vector<Prevail>*> precond_vectors{
            &g_operators[op_index].get_prevail_start(),
            &g_operators[op_index].get_prevail_overall(),
            &g_operators[op_index].get_prevail_end()
    };
    vector<const vector<PrePost>*> prepost_vectors{
            &g_operators[op_index].get_pre_post_start()
    };
    checker(prepost_vectors, precond_vars);
    checker(precond_vectors, precond_vars);

    for (int i = 0; i < g_variable_domain.size(); i++) {
        if (precond_vars[i] == 1) {
            double dist = find_short_path(g_transition_graphs[i], g_shortest_costs_tables, temporal_fd_last_grounded(i), temporal_fd_first(op_index, i), g_initial_state->state, i);
            all_dist.push_back(dist + temporal_fd_l_at(op_index, i));
        }
    }
    return all_dist;
}

double temporal_fd_grounded_action_dist(int op_index) {
    vector<double> all_dist = temporal_fd_grounded_action_dist_interm(op_index);
    if (all_dist.size() != 0)
        return double(*max_element(begin(all_dist), end(all_dist)));
    else return -10;

    //select maximum from those distances and return;
    //the same for usual action...
}

double temporal_fd_grounded_action_dist_sum(int op_index) {
    vector<double> all_dist = temporal_fd_grounded_action_dist_interm(op_index);
    if (all_dist.size() != 0)
        return double (accumulate(all_dist.begin(), all_dist.end(),0.0));
    else return -10;

    //select maximum from those distances and return;
    //the same for usual action...
}

vector<double> temporal_fd_action_dist_interm(int op_index1, int op_index2) {
    vector<double> all_dist;
    int precond_vars[g_variable_domain.size()];
    fill_n(precond_vars, g_variable_domain.size(), -1);
    int all_invovl_vars[g_variable_domain.size()];
    fill_n(all_invovl_vars, g_variable_domain.size(), -1);
    //preconditions for the second action

    vector<const vector<Prevail>*> precond_all_inv_vectors{
        &g_operators[op_index1].get_prevail_start(),
        &g_operators[op_index1].get_prevail_overall(),
        &g_operators[op_index1].get_prevail_end()
    };
    vector<const vector<PrePost>*> prepost_all_inv_vectors{
        &g_operators[op_index1].get_pre_post_start(),
        &g_operators[op_index1].get_pre_post_end()
    };
    vector<const vector<Prevail>*> precond_vectors{
        &g_operators[op_index2].get_prevail_start(),
        &g_operators[op_index2].get_prevail_overall(),
        &g_operators[op_index2].get_prevail_end()
    };
    vector<const vector<PrePost>*> prepost_vectors{
        &g_operators[op_index2].get_pre_post_start()
    };

    checker(prepost_vectors, precond_vars);
    checker(precond_vectors, precond_vars);
    checker(precond_all_inv_vectors, all_invovl_vars);
    checker(prepost_all_inv_vectors, all_invovl_vars);

    //processing condtioning
    for (int i = 0; i < g_variable_domain.size(); i++) {
        if (precond_vars[i] == all_invovl_vars[i] and precond_vars[i] == 1) {
            double dist = find_short_path(g_transition_graphs[i], g_shortest_costs_tables, temporal_fd_last(op_index1, i),
                                          temporal_fd_first(op_index2, i), g_initial_state->state, i);
            all_dist.push_back(dist + temporal_fd_l_at(op_index1, i) - temporal_fd_f_at(op_index2, i));
        }
    }
    return all_dist;
}

double temporal_fd_action_dist_sum(int op_index1, int op_index2) {
    vector<double> all_dist = temporal_fd_action_dist_interm(op_index1, op_index2);
    if (all_dist.size() != 0)
        return double (accumulate(all_dist.begin(), all_dist.end(),0.0));
    else return -10;
}

double temporal_fd_action_dist(int op_index1, int op_index2) {
    vector<double> all_dist = temporal_fd_action_dist_interm(op_index1, op_index2);
    if (all_dist.size() != 0)
        return double(*max_element(std::begin(all_dist), std::end(all_dist)));
    else return -10;

}

double temporal_fd_dur(int op_index) {
    return g_initial_state->state[g_operators[op_index].get_duration_var()];
}
