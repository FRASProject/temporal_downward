#include "globals.h"
#include "operator.h"
#include "state.h"
#include "successor_generator.h"

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

string SuccessorGeneratorSwitch::type_generator() const {
    return "SuccessorGeneratorSwitch";
}

string SuccessorGeneratorGenerate::type_generator() const {
    return "SuccessorGeneratorGenerate";
}

SuccessorGeneratorSwitch::SuccessorGeneratorSwitch(istream &in)
{
    cin >> switch_var;
    immediate_ops = read_successor_generator(in);
    for(int i = 0; i < g_variable_domain[switch_var]; i++)
        generator_for_value.push_back(read_successor_generator(in));
    default_generator = read_successor_generator(in);
}

void SuccessorGeneratorSwitch::generate_applicable_ops(
        const TimeStampedState &curr, vector<const Operator *> &ops)
{
    immediate_ops->generate_applicable_ops(curr, ops);
    generator_for_value[static_cast<int>(curr[switch_var])]->generate_applicable_ops(curr, ops);
    default_generator->generate_applicable_ops(curr, ops);
}

void SuccessorGeneratorSwitch::_dump(string indent)
{
    cout << indent << "switch on " << g_variable_name[switch_var] << endl;
    cout << indent << "immediately:" << endl;
    immediate_ops->_dump(indent + "  ");
    for(int i = 0; i < g_variable_domain[switch_var]; i++) {
        cout << indent << "case " << i << ":" << endl;
        generator_for_value[i]->_dump(indent + "  ");
    }
    cout << indent << "always:" << endl;
    default_generator->_dump(indent + "  ");
}

void SuccessorGeneratorGenerate::generate_applicable_ops(
        const TimeStampedState &, vector<const Operator *> &ops)
{
    ops.insert(ops.end(), op.begin(), op.end());
}

SuccessorGeneratorGenerate::SuccessorGeneratorGenerate(istream &in)
{
    int count;
    cin >> count;
    for(int i = 0; i < count; i++) {
        int op_index;
        in >> op_index;
        op.push_back(&g_operators[op_index]);
    }
}

void SuccessorGeneratorGenerate::_dump(string indent)
{
    for(int i = 0; i < op.size(); i++) {
        cout << indent;
        op[i]->dump();
    }
}

SuccessorGeneratorGenerate::~SuccessorGeneratorGenerate() = default;


SuccessorGeneratorSwitch::~SuccessorGeneratorSwitch() {
    delete(immediate_ops);
    for (auto const gen: generator_for_value) {
        delete (gen);
    }
    delete(default_generator);
}

SuccessorGenerator *read_successor_generator(istream &in)
{
    string type;
    in >> type;
    if(type == "switch") {
        return new SuccessorGeneratorSwitch(in);
    } else if(type == "check") {
        return new SuccessorGeneratorGenerate(in);
    }
    cout << "Illegal successor generator statement!" << endl;
    cout << "Expected 'switch' or 'check', got '" << type << "'." << endl;
    exit(1);
}

string return_type_generator(SuccessorGenerator* sg) {
    return sg->type_generator();
}

//void delete_inner_gen(SuccessorGenerator* sg) {
//    if (return_type_generator(g_successor_generator) == "SuccessorGeneratorGenerate") {
//        SuccessorGeneratorGenerate* sggen = dynamic_cast<SuccessorGeneratorGenerate*>(sg);
//        for (const Operator *op: sggen->op)
//            delete (op);
//
//    }
//
//}
