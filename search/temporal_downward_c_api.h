//
// Created by xankr on 02.12.2021.
//

#ifndef TEMPORALFASTDOWNWARD_TEMPORAL_DOWNWARD_C_API_H
#define TEMPORALFASTDOWNWARD_TEMPORAL_DOWNWARD_C_API_H
#ifdef __cplusplus
extern "C"
{
#endif

void temporal_fd_init_all(const char *input);

void temporal_fd_deinit();

double temporal_fd_grounded_action_dist(int op_index);

double temporal_fd_action_dist(int op_index1, int op_index2);

double temporal_fd_grounded_action_dist_sum(int op_index);

double temporal_fd_action_dist_sum(int op_index1, int op_index2);

double temporal_fd_dur(int op_index);



#ifdef __cplusplus
}
#endif
#endif //TEMPORALFASTDOWNWARD_TEMPORAL_DOWNWARD_C_API_H
