#ifndef DOMAIN_TRANSITION_GRAPH_SHORTPATH_H
#define DOMAIN_TRANSITION_GRAPH_SHORTPATH_H

#include "operator.h"
#include "domain_transition_graph.h"
#include "map"


double find_short_path(DomainTransitionGraph* &transition_graphs, map<int, vector<int*>> &shortest_costs_tables, int state1, int state2, const vector<double> &init_states, int var_num);


#endif
