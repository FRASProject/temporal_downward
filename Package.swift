// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "temporal_downward",
    products: [
        .library(
            name: "temporal_downward",
            type: .dynamic,
            targets: ["temporal_downward"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "temporal_downward",
            dependencies: [],
            path: "search",
            exclude: ["CMakeLists.txt", "epsilonize_plan.py", "Makefile", "Makefile.depend"],
            publicHeadersPath: "include"
        ),
    ],
    cxxLanguageStandard: .cxx11
)
